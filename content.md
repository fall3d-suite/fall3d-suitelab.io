---
title: FALL3D Suite Website
...

## Updates

> **New release** <br>
> 14 November 2023 <br>
> FALL3D version 8.3 released!

---

## Overview

**FALL3D** is an Eulerian model for atmospheric passive transport and deposition based on the so-called advection–diffusion–sedimentation (ADS) equation. The code version 8.x has been redesigned and rewritten from scratch in order to overcome legacy issues and allow for successive optimisations in the preparation towards extreme-scale computing. The new versions include significant improvements from the point of view of model physics, numerical algorithmic methods, and computational efficiency. In addition, the capabilities of the model have been extended by incorporating new features such as the possibility of running ensemble forecasts and dealing with multiple atmospheric species (i.e. volcanic ash and gases, mineral dust, and radionuclides). Ensemble run capabilities are supported since version 8.1, making it possible to quantify model uncertainties and improve forecast quality.

## User guide

The user guide can be found at [User Guide](https://fall3d-suite.gitlab.io/fall3d)

## Repositories

* [FALL3D](https://gitlab.com/fall3d-suite/fall3d): FALL3D general purpose version
* [FALL3D-GPU](https://gitlab.com/fall3d-suite/fall3d-gpu): FALL3D GPU-accelerated version

## What’s new

- Improved management of memory. Restrictions for reading large meteorological files (>100Gb) were solved
- The `SetDbs` task was re-written from scratch and new meteorological datasets were included
- List of meteorological models available:
  - `WRF-ARW`: The Advanced Research WRF (ARW) (mesoscale model)
  - `GFS`: The Global Forecast System (global weather forecast model)
  - `GEFS`: The Global Ensemble Forecast System (global weather forecast model)
  - `ERA5`: The ERA5 ECMWF reanalysis in pressure levels (global climate and weather renalysis)
  - `ERA5ML`: The ERA5 ECMWF reanalysis in model levels (global climate and weather renalysis)
  - `IFS`: The ECMWF Integrated Forecasting System (global weather forecast model)
  - `CARRA`: The Copernicus Arctic Regional Reanalysis (Arctic regional reanalysis )

## New options in the configuration file

- `MEMORY_CHUNK_SIZE`: Size of memory chunks used to store meteo data timesteps. Must be greater than 1
- `METEO_ENSEMBLE_BASEPATH`: Root path for meteo input file. Used for ensemble runs when multiple meteo files are available
- `RESTART_ENSEMBLE_BASEPATH`: Root path for restart file. Used for ensemble runs when multiple restart files are available 

## Example 1: CARRA

* A hypothetical eruption in Grindavík (Iceland) has been simulated
* 1-month simulation for Iceland using the 2-km CARRA dataset
* CARRA reanalysis from November 2022 was used
* A low-level column (~2 km) was assumed

![](so2_colmass_iceland.mp4){height=400 autoplay='' muted=''}

### Performance in MN4

| Task          | Input file | Output file | Time (s)       |
| ------------- | ---------- | ----------- | -------------- |
| SetDBS+FALL3D | 100GB      | 29GB        | 1420 (505+915) |
| ALL           | 100GB      | 0.84GB      | 1237           |

## Example 2: GEFS

* Klyuchevskoy eruption (Kamchatka) driven by GEFS data (30-member ensemble)
* The meteorological ensemble introduces more variability than the source term perturbation!

![](so2_colmass.mp4){height=400 autoplay='' muted=''}

